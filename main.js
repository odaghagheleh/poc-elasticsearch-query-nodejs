
var express = require('express');
var app = express();

var bodyParser = require('body-parser');


// Create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: true, limit: '50mb' });
// app.use();

app.use(express.static('public'));
app.get('/index.html', function (req, res) {
    res.sendFile(__dirname + "/" + "index.htm");
})

app.post('/process_post', urlencodedParser, function (req, res) {

    var messege = JSON.parse(req.body.jsonInput);
    var num_of_time = req.body.num_of_time;

    while (num_of_time > 0) {

        for (let index = 0; index < messege.length; index++) {
            const element = messege[index];
            console.log(element);
        }

        num_of_time--;
    }

    // Prepare output in JSON format
    response = { "Request": "ok" };
    console.log(response);
    res.end(JSON.stringify(response));
})

var server = app.listen(8080, function () {
    var host = server.address().address
    var port = server.address().port

    console.log("Example app listening at http://%s:%s", host, port)

})